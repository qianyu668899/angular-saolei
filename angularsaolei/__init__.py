from pyramid.config import Configurator
from pyramid.session import UnencryptedCookieSessionFactoryConfig
from pyramid.session import SignedCookieSessionFactory

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    my_session_factory = UnencryptedCookieSessionFactoryConfig('itsaseekreet', 0)
    my_session_factory2 = SignedCookieSessionFactory('itsaseekreet')
    config = Configurator(settings=settings, session_factory=my_session_factory2)
    config.include('pyramid_chameleon')
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    ##Add my route config here
    config.add_route('api.init', '/api/init')
    config.add_route('api.update', '/api/update')
    config.add_route('api.board', '/api/board')
    config.add_route('api.state', '/api/state')
    config.add_route('api.mines', '/api/mines')
    config.add_route('api.number', '/api/number')
    config.add_route('api.revealed', '/api/revealed')
    config.add_route('api.flag', '/api/flag')
    ##
    config.scan()
    return config.make_wsgi_app()
